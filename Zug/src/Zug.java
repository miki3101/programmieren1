
public class Zug // kennt den ersten, letzten und zugl�nge
{
	private Waggon erster; // variablen vom Zug
	private Waggon letzter; // variablen vom Zug
	private int zuglaenge = 0; // gibt die Zugl�nge an (wenn sie fix vorgegeben ist), w�rde sowieso auf null
								// gesetzt

	public Zug() // damit man mal einen Zug hat, soll leer sein
	{
	}
	// zuerst brauch ich einmal einen neuen Waggon (if (erster == null)
	// den neuen waggon muss ich mir als "erster" merken (Variable die auf den
	// ersten zeigt)
	// den neuen waggon muss ich mir auch als "letzter" merken (Variable die auf den
	// letzten zeigt)

	// wenn ich einen zweiten dazu gebe
	// brauch ich wieder einen neuen Waggon
	// diesen muss ich beim "letzten" einh�ngen (else, letzter einh�ngen, neuer ist
	// letzter)
	// dieser ist dann der neue "letzte"

	// wenn ich einen dritten dazu gebe
	// diesen wieder beim "letzten" einh�ngen (zweite wird nicht weggeschmissen,
	// denn der letzte zeigt auf diesen)
	// dieser ist dann der neue "letzte"

	public void neuerWaggon(String inhalt)
	{
		Waggon neuerWaggon = new Waggon(inhalt); // einen neuen Waggon erstellt (kommt darauf an, wieviele schon da
													// sind)

		// der zug ist leer
		if (erster == null) // null => die Referenz zeigt noch ins Leere
		{
			erster = neuerWaggon;
			letzter = neuerWaggon;
		} else // es gibt schon einen Waggon
		{
			letzter.einhaengen(neuerWaggon); // zweite wurde mit erstem verbunden
			letzter = neuerWaggon; // der neue ist der neue letzte
		}

		zuglaenge++; // z�hlt einen zur zugl�nge dazu

	}

	public int length() // um die ganze l�nge in zahlen auch noch auszugeben
	{
		return zuglaenge;  // gibt den wert der zugl�nge zur�ck
	}

	public String toString()
	{
		if (erster == null)
		{
			return "0";

		} else
		{
			String ergebnis = zuglaenge + ":";

			Waggon temp = erster; // temp ist der erste Waggon

			while (temp != null) // solange er auf einem Waggon steht, muss er zum n�chsten schauen
			{
				ergebnis = ergebnis + " " + temp.toString(); // gibt mir von ersten bis letzten waggon aus
				temp = temp.nachbar(); // der temp wert wechselt zum nachbar
			}

			return ergebnis; // gibt den wert zur�ck den der gesamten Zug hat
		}
	}

	public String waggonInhaltAnStelle(int stelle)
	{
		if (stelle > zuglaenge - 1 || stelle < 0) //es darf keine stelle eingebeben werden, die gr��er ist (|| hei�t oder)
		{							// als meine zugl�nge (-1, weil wir bei 0 anfangen zum z�hlen)
			return "Stelle nicht vorhanden!";	 // dann gib keinen Wert zur�ck
		}
		
		Waggon temp = erster; // erste stelle ist der erste waggon

		for (int springen = 0; springen < stelle; springen++) //fang bei 0 an, und spring solange weiter 
		{													 // bis du bei der richtigen stelle bist
			temp = temp.nachbar(); //zum n�chsten springen
		}

		return temp.toString(); // gibt mir den wert zur�ck, zu dem er gesprungen ist
	}
}
