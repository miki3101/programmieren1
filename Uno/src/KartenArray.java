import java.util.ArrayList;

import org.campus.uno.Farbe;
import org.campus.uno.Karte;

public class KartenArray
{

	public static void main(String[] args)
	{
		Karte[] meineKarten = new Karte[] //ich definiere meine Karten in der Hand mit einem Array
				{new Karte(Farbe.rot, 1), new Karte(Farbe.blau, 1), new Karte(Farbe.gelb, 10)};
		
		System.out.println(meineKarten[1]);
		
		for (Karte karte : meineKarten) //damit er einmal durch das Karten Array durchgeht
		{
			System.out.println(karte); // damit er alle Karten ausgibt
		}
		
		ArrayList<Karte> meineListe = new ArrayList<Karte>(); //man kann damit Typen <Zahlen, Z�ge, Karten> verwalten (Array List)
		
		meineListe.add(new Karte(Farbe.rot,2)); // eine Karte in die Liste einf�gen automatisch hinten dran/
												// meineListe.add(0, new Karte ()); w�rde an stelle 0 hinzugef�gt
		meineListe.add(new Karte(Farbe.blau,1)); // wird der Reihe nach einsortiert, wieder bei 0 starten
		meineListe.add(new Karte(Farbe.gelb,10));
		
		System.out.println(meineListe.get(1)); // um die bestimmte Position auszugeben
		
		for (Karte karte : meineListe) // w�re das gleiche um die Liste auszugeben
		{
			System.out.println(karte); // gibt die gesamte Liste aus
		}
		
		meineListe.size(); // wie viele Dinge (Karten) sind in dieser Liste
		
		System.out.println(meineListe.size()); // gibt aus, wieviele Karten in der Liste sind
		
		System.out.println(meineListe.remove(0)); // nimmt die Karte aus der Liste, die an der Stelle 0 steht
		
		System.out.println(meineListe.size()); // sehe wie viele Karten noch da sind
		
		System.out.println(meineListe); // wenn ich die Elemente einer Liste ausgeben m�chte
										// bei einer Liste erspare ich mir die toString Methode		
	}

}
