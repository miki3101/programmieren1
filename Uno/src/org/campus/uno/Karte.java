package org.campus.uno;

public class Karte
{
	private Farbe farbe; // hat die Eigenschaft Farbe
	private int zahl; // hat die Eigenschaft Zahl

	public Karte(Farbe kartenfarbe, int zahlenwert)
	{
		farbe = kartenfarbe; // ich kann dem typ farbe eine farbe zuteilen
		zahl = zahlenwert; // ich kann dem typ zahl eine zahl zuteilen
	}

	public boolean match(Karte passt) // signatur einer Methode (public, returnwert, name (parameter)
	{
		if (farbe == passt.farbe) // ist die Farbe der 1. Karte die gleiche, wie die der 2. Karte
			return true;

		if (zahl == passt.zahl) // ist die Zahl der 1. Karte die gleiche, wie die der 2. Karte
			return true;

		return false; // wenn nicht dann gibt false aus
	}

	public boolean compare(Karte zweiteKarte)
	{
		if (farbe == zweiteKarte.farbe) // schaut ob sie die gleiche Farbe haben
		{
			boolean result = zahl < zweiteKarte.zahl; // vergleicht welche Karte gr��er ist
			return result;
			// return zahl < zweiteKarte.zahl; //kurze Schreibweise
		} else // wenn die farbe der 1. Karte nicht gleich ist, kontrolliert er die
		{
			// ordinal gibt einen Zahlenwert mit der Stelle im enum zur�ck
			return farbe.ordinal() < zweiteKarte.farbe.ordinal(); // farbenreihenfolge wird in enum definiert, 1. ist
																	// die kleinste
		}
	}

	public String toString() // gib immer beides aus
	{
		return farbe + " " + zahl;
	}
}
