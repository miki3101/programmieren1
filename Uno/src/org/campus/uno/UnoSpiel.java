package org.campus.uno;

import java.util.ArrayList;
import java.util.Collections;

public class UnoSpiel
{
	// Ablege Stapel und Karten Stapel erstellt
	private ArrayList<Karte> ablageStapel = new ArrayList<Karte>();
	private ArrayList<Karte> kartenStapel = new ArrayList<Karte>();

	private ArrayList<Spieler> mitspieler = new ArrayList<Spieler>();

	// Konstruktor
	public UnoSpiel()
	{
		// wir haben 2 leere Kartenstapel und die bef�llen man jetzt

		for (int index = 0; index < 2; index++) // damit ich von jeder Karte 2 St�ck habe
		{
			for (int zahlenwert = 0; zahlenwert < 10; zahlenwert++) // damit er das 10 rote, blaue, gr�ne und gelbe
																	// hinzuf�gt
			{
				kartenStapel.add(new Karte(Farbe.rot, zahlenwert));
				kartenStapel.add(new Karte(Farbe.blau, zahlenwert));
				kartenStapel.add(new Karte(Farbe.gruen, zahlenwert));
				kartenStapel.add(new Karte(Farbe.gelb, zahlenwert));
			}
		}

		Collections.shuffle(kartenStapel); // mischt die Karten (statisch weil, ich kein Objekt daf�r brauche)
	}

	public Karte abheben()
	{
		kartenStapel.remove(0); // eine Karte wird vom Stapel abgehoben

		return kartenStapel.remove(0); // er gibt den wert zur�ck ohne dieser Karte
	}

	public void mitspielen(Spieler neuerSpieler)
	{
		mitspieler.add(neuerSpieler); // mit dieser Methode f�gt man einen neuen Spieler, der Liste Mitspieler hinzu
	}

	public void austeilen()
	{
		for (int index = 0; index < 7; index++) // jeder Spieler zieht 7 mal eine Karte
		{
			for (Spieler sp : mitspieler) // jeder Spieler kommt einmal dran
			{
				Karte karte = abheben(); // ich sage das der Spieler eine Karte abhebt
				sp.aufnehmen(karte); // hab ich in der Klasse Spieler definiert / Spieler nimmt karte in die Hand
			}
		}
		Karte temp = abheben(); // ich hebe noch eine Karte ab
		ablageStapel.add(temp); // ich lege eine Karte auf den Ablagestapel
	}
}
