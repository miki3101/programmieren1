

import org.campus.uno.Farbe;
import org.campus.uno.Karte;
import org.campus.uno.Spieler;
import org.campus.uno.UnoSpiel;

public class UnoApp
{
	public static void main(String[] args)
	{
		UnoSpiel meinSpiel = new UnoSpiel(); // ich mache ein neues UnoSpiel
		
		Karte abgehobeneKarte = meinSpiel.abheben(); //in diesem Spiel verwende ich die Methode abheben
		
		Spieler sp1 = new Spieler("Susi"); // f�ge neuen Spieler hinzu
		Spieler sp2 = new Spieler("Strolchi"); 
		Spieler sp3 = new Spieler("Fauchi");
		
		meinSpiel.mitspielen(sp1); // ich f�ge die Spieler dem Spiel hinzu (dieser Spieler spielt mit)
		meinSpiel.mitspielen(sp2);
		meinSpiel.mitspielen(sp3);
		
		meinSpiel.austeilen(); // jeder Spieler erh�lt 7 Karten vom Stapel
		

		
		
		System.out.println(sp1.passendeKarte(new Karte(Farbe.rot, 1))); // man gibt dem neuen Spieler eine Karte und schaut ob sie passt
																		// wenn er eine Handkarte hat, die passt, gibt er sie aus
																		// wenn nicht, gibt er den Wert null aus
	}

}
