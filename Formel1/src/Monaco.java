
public class Monaco
{

	public static void main(String[] args)
	{
		Auto auto1 = new Auto("rot"); // neuen Datentyp Auto erstellt - rot unter "" weil es ein String ist
		Auto auto2 = new Auto("blau");
		Auto auto3 = auto1; // Auto3 ist das selbe wie das auto1 / 2 Objekte (auto1, auto2) und 2 Variablen
							// f�r 1 Objekt (auto1)

		auto1.status();
		auto1.volltanken();
		auto2.volltanken();
		auto1.status();
		auto2.status();
		auto3.status();

		auto1.beschleunigen(10);
		auto1.status();

		auto1.fahren(120);
		auto1.status();


	}


}
