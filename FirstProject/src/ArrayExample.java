
public class ArrayExample
{

	public static void main(String[] args)
	{
		int [] feld1 = new int [] {1, 2, 3, 4, 5, 6};
		
		System.out.println(feld1[3]); // wert an der stelle 3
		
		int stelle=4;
		System.out.println(feld1[stelle]); // index aus einer variable
		
		System.out.println(feld1.length);
		
		printArray(feld1);
		MaxFinden.findMax(feld1); //Verweis auf eine andere Klasse
		
		System.out.println();
		printArray(feld1); // Ausgabe vor dem Tauschen
		swap(feld1, 2, 3); // Tauschen
		printArray(feld1); // nach dem Tauschen
		
	}
	
	public static void printArray(int[] feld1)
	{
		for(int index=0; index<feld1.length; index++) // f�r die gesamte l�nge des Feldes
		{
			System.out.print(feld1[index] + " "); // alle werte an jeder stelle ausgeben // nicht in einer linie
		}
		System.out.println();

	}

	//schreiben Sie eine Methode, die in einem Feld zwei Stellen vertauscht
	// die Methode hat das Feld und die erste und zweite Stelle als Parameter

	
	public static void swap(int [] feld, int a, int b) //Methode f�rs tauschen
	{
		
		int temp = feld[a]; // wert von Feld a wird gemerkt
		
		feld[a] = feld [b]; // wert von feld a, wird mit Wert von Feld b �berschrieben
		feld [b] = temp; // gemerkter Wert , kommt in Feld b
		
	}
}


