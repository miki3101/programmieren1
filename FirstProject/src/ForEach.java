
public class ForEach
{

	public static void main(String[] args)
	{
		int[] zahlen = new int[]
		{ 1, 3, 5, 1, 7, 22, 33, 42 };
		// name der variable : feld ist das ich durchlaufen will
		for (int wert : zahlen) // die wir so oft durchlaufen, wie das feld lang ist
		{
			System.out.println(wert); // bei jedem Durchlauf hat Wert den Zahlen Wert an der Stelle im feld
		}

		int summe = 0; // merkt sich was die summe, nach jeder Zahl ist

		for (int Zahlenwert : zahlen) // Zahlenwert, kann ich schreiben was ich will (schaut wert von jeder Stelle an)
		{
			summe = summe + Zahlenwert; // rechnet jede einzelne Zahl �ber die er dr�ber schaut zusammen
		}

		System.out.println("Summe ist gleich: " +summe); // gibt die entg�ltige Summe aus
		
	}

}
