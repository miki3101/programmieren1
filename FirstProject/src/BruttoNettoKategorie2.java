
public class BruttoNettoKategorie2
{
	public static void main(String args[])
	{
		double netto = 100;
		int steuerkategorie = 3; // 2 oder 3
		double brutto = netto;
		
		brutto = brutto(100, 2); // aufrufen der Methode
		System.out.println(brutto);
		brutto = brutto (200, 1);
		System.out.println(brutto);

		switch (steuerkategorie)
		{
		case 1:
			brutto = netto * 1.2; // 20% Steuer
			break;
		case 2:
			brutto = netto * 1.12; // 12% Steuer
			break;
		case 3:
			brutto = netto * 1.1; // 10% Steuer
			break;

		default:
			brutto = netto;
			break;
		}

		System.out.println(brutto);

	}

	// datentyp
	// r�ckgabewert        namen	 void	kein r�ckgabe wert
	public static double brutto(double netto, int steuerkategorie) // parameter die ich von aussen mitgebe
	{
		double brutto; // variablen die ich innerhalb der Methode brauche
		switch (steuerkategorie)
		{
		case 1:
			brutto = netto * 1.2; // 20% Steuer
			System.out.println("mit 20%");
			break;
		case 2:
			brutto = netto * 1.12; // 12% Steuer
			System.out.println("mit 12%");
			break;
		case 3:
			brutto = netto * 0.11; // 10% Steuer
			System.out.println("mit 10%");
			break;

		default:
			brutto = netto;
			break;
		}
		return brutto; // return liefert den wert zur�ck
	}
}
