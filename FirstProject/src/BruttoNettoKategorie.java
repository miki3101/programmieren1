
public class BruttoNettoKategorie
{
	public static void main(String args[])
	{
		double netto = 100;
		int steuerkategorie = 3; // 2 oder 3
		double brutto = netto;

		if (steuerkategorie == 1)
		{
			brutto = netto * 1.2; // 20% Steuer
			System.out.println("mit 20%");
		} else if (steuerkategorie == 2)
		{
			brutto = netto * 1.12; // 12% Steuer
			System.out.println("mit 12%");
		} else if (steuerkategorie == 3)
		{
			brutto = netto * 1.1; // 10 % Steuer
			System.out.println("mit 10%");
		}
		System.out.println(brutto);

	}
}
