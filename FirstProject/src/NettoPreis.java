
public class NettoPreis
{

	public static void main(String[] args)
	{
		double brutto = 100;
		int steuerkategorie = 1;
		double netto = brutto;

		netto = netto(100, 1);
		netto = netto(100, 2);
		netto = netto(100, 3);

		switch (steuerkategorie)
		{
		case 1:
			netto = brutto / 1.2; // 20% Steuer
			break;
		case 2:
			netto = brutto / 1.12; // 12% Steuer
			break;
		case 3:
			netto = brutto / 1.1; // 10% Steuer
			break;

		default:
			netto = brutto;
			break;
		}

		System.out.println(netto);

	}

	public static double netto(double brutto, int steuerkategorie)
	{
		double netto;
		switch (steuerkategorie)
		{
		case 1:
			netto = brutto / 1.2; // 20% Steuer
			break;
		case 2:
			netto = brutto / 1.12; // 12% Steuer
			break;
		case 3:
			netto = brutto / 1.1; // 10% Steuer
			break;

		default:
			netto = brutto;
			break;
		}
		return netto;
	}
}
